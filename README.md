<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://git.rwth-aachen.de/lu.ostendorf/flipdot_destination_display">
    <img src="/assets/flipdot_logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Flipdot Destination Display</h3>

  <div align="center">
    Modification of an AEG/Annax destination display from a berlin city train to a multifunctional wifi enabled display.
    <br />
    <a href="https://git.rwth-aachen.de/lu.ostendorf/flipdot_destination_display"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://git.rwth-aachen.de/lu.ostendorf/flipdot_destination_display">View Demo</a>
    ·
    <a href="https://git.rwth-aachen.de/lu.ostendorf/flipdot_destination_display/issues">Report Bug</a>
    ·
    <a href="hhttps://git.rwth-aachen.de/lu.ostendorf/flipdot_destination_display/issues">Request Feature</a>
  </div>
</div>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#recources">Similar Projects and Recources</a>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

In this repository you can find a collection of files I created to communicate with my AEG/Annax destination display. Since this was a hobby/learning project I made for myself in 2018 it is neither well documented nor exeptionally well written.

I connected the bus port of the display to an ESP8266 using a software serial library to simulate control units of the city train (You can find the corresponding arduino library in /libraries/IBIS). The code is based on information I gained mainly through this website: <a href="http://www.ibis-world.de/">IBIS World</a>

To get something usefull out of the display I created two libraries to pull my calendar from GoogleCalendar and the local weather from YahooWeather (You can also find these arduino libraries in /libraries). To control the displayed contents, I added alexa support with the help of this library: <a href="https://github.com/vintlabs/fauxmoESP">fauxmoESP</a>.

**Here is a little demo of what the final thing looked like:**
<div align="center">
![Alexa Demo](/pictures/video.mp4)

<a href="https://git.rwth-aachen.de/lu.ostendorf/flipdot_destination_display">
  <img src="/pictures/backside.jpg" alt="backside" width="700">
</a>
</div>

<!-- RECOURCES -->
## Similar Projects and Recources
- Forum all about old Flipdot Diplays: <a href="http://www.ibis-world.de/">IBIS World</a>
- Yahoo Weather API: <a href="https://developer.yahoo.com/weather/">Yahoo Weather</a>
- Google Calendar API: <a href="https://developers.google.com/calendar">Google Calendar</a>
- Add Alexa Voice Control to ESP: <a href="https://github.com/vintlabs/fauxmoESP">fauxmoESP</a>
- A way more sophisticated library for flipdot control by one of the forum members: <a href="https://github.com/Mezgrman/flipdot">Mezgrman</a>
