/*
  Kalender.h - Library for receiving Calendar Events from Google.
  Created by Lukas Ostendorf, Mai 17, 2018.
*/

#ifndef Kalender_h
#define Kalender_h

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <WiFiClientSecureRedirect.h>


class Kalender
{
  public:
    Kalender();
    void getKalender();
    String getGanztaegig();
    String getTimeAktuell();
    String getEventAktuell();
    int getDayAktuell();
    String getWeekDayAktuell();
    String getMonthAktuell();
    int istAktuell();
    String getTimeNaechstes();
    String getEventNaechstes();
    int getDayNaechstes();
    String getWeekDayNaechstes();
    String getMonthNaechstes();
    String getTime();
    int getDay();
    int getYear();
    String getWeekDay();
    String getMonth();

  private:
    int getIDAktuell();
    int getIDNaechstes();
    String Monat(String monat);
    String Wochentag(String wochentag);
    char const * const dstHost = "script.google.com";
    char const * const dstPath = "your path";  // ** UPDATE ME **
    int const dstPort = 443;
    int32_t const timeout = 5000;
    char const * const dstFingerprint = "C7:4A:32:BC:A0:30:E6:A5:63:D1:8B:F4:2E:AC:19:89:81:20:96:BB";
    char const * const redirFingerprint = "E6:88:19:5A:3B:53:09:43:DB:15:56:81:7C:43:30:6D:3E:9D:2F:DE";
    String WeekDay[5] ={"","","","",""};
    int Day[5] = {0,0,0,0,0};
    String Month[5] = {"","","","",""};
    int Year[5] = {0,0,0,0,0};
    String Time[5] = {"","","","",""};
    String Event[5] = {"","","","",""};
    int i=0;
};

#endif
