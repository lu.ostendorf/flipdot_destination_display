/*
  Kalender.cpp - Library for receiving Calendar Events from Google.
  Created by Lukas Ostendorf, Mai 17, 2018.
*/

#include "Arduino.h"
#include "Kalender.h"
#include <ESP8266WiFi.h>
#include <WiFiClientSecureRedirect.h>

Kalender::Kalender() {
}

// Function for retrieving calendar events from google calendar
void Kalender::getKalender() {
    bool error = true;
    WiFiClientSecureRedirect client;

    do {
        if (client.connect(dstHost, dstPort) != 1) {    // send connect request
            break;
        }
        while (!client.connected()) {                   // wait until connected
            client.tick();
        }
        if (client.request(dstPath, dstHost, 2000, dstFingerprint, redirFingerprint) != 0) {    // send alarm request
            break;
        }
        while (!client.response()) {                    // wait until host responded
            client.tick();
        }
        while (client.available()) {                    // read and print until end of data
            WeekDay[i] = client.readStringUntil(' ');
            Month[i] = client.readStringUntil(' ');
            Day[i] = client.parseInt();
            Year[i] = client.parseInt();
            Time[i] = client.readStringUntil('G');
            Time[i].replace(" ", "");
            Time[i].replace("00:00:00", "ganztägig");
            Time[i].replace(":00", "");
            if (Time[i].length() <= 2)
                Time[i].concat(":00");
            if (client.find("undefined\t")) {
                Event[i] = client.readStringUntil('\n');
            }
            if (i >= 4)
                break;
            i++;
        }
        i = 0;
        client.stop();
        error = false;
    } while (0);
}

// Function to translate month names into german language
String Kalender::Monat(String monat) {
    monat.replace("Jan", "Jan");
    monat.replace("Feb", "Feb");
    monat.replace("Mar", "Mär");
    monat.replace("Apr", "Apr");
    monat.replace("May", "Mai");
    monat.replace("Jun", "Jun");
    monat.replace("Jul", "Jul");
    monat.replace("Aug", "Aug");
    monat.replace("Sep", "Sep");
    monat.replace("Oct", "Okt");
    monat.replace("Nov", "Nov");
    monat.replace("Dec", "Dez");
    return monat;
}

// Function to translate day names into german language
String Kalender::Wochentag(String wochentag) {
    wochentag.replace("Mon", "Mo.");
    wochentag.replace("Tue", "Di.");
    wochentag.replace("Wed", "Mi.");
    wochentag.replace("Thu", "Do.");
    wochentag.replace("Fri", "Fr.");
    wochentag.replace("Sat", "Sa.");
    wochentag.replace("Sun", "So.");
    return wochentag;
}

// Function for getting events scheduled for the whole day
String Kalender::getGanztaegig() {
    if (Time[1].equals("ganztägig"))
        return Event[1];
    else
        return "Keine Termine";
}

// Function for getting the ID of the current event
int Kalender::getIDAktuell() {
    boolean ganz = true;
    int k = 1;
    while (ganz == true) {
        if (!Time[k].equals("ganztägig")) {
            return k;
            ganz == false;
        }
        if (k == 4) {
            ganz == false;
            return 0;
        }
        k++;
    }
}

// Function for getting the time of the current event
String Kalender::getTimeAktuell() {
    if (!Event[0].equals("heute"))
        return (Event[0] + " " + Time[getIDAktuell()]);
    else
        return (Time[getIDAktuell()]);
}

// Function for getting the current event
String Kalender::getEventAktuell() {
    if (Event[getIDAktuell()] == "")
        return "Keine Termine";
    else
        return Event[getIDAktuell()];
}

// Function for getting the current day
int Kalender::getDayAktuell() {
    return Day[getIDAktuell()];
}

// Function for getting the current weekday
String Kalender::getWeekDayAktuell() {
    return Wochentag(WeekDay[getIDAktuell()]);
}

// Function for getting the current month
String Kalender::getMonthAktuell() {
    return Monat(Month[getIDAktuell()]);
}

// Function to check if an event is currently running
int Kalender::istAktuell() {
    if (Event[0].equals("heute"))
        return 0;
    if (Event[0].equals("Um"))
        return 1;
    if (Event[0].equals("Seit"))
        return 2;
    else
        return 0;
}

// Function for getting the ID of the next event
int Kalender::getIDNaechstes() {
    boolean ganz = true;
    int k = 1;
    while (ganz == true) {
        if (!Time[k].equals("ganztägig")) {
            return k + 1;
            ganz == false;
        }
        if (k == 4) {
            ganz == false;
            return 0;
        }
        k++;
    }
}

// Function for getting the time of the next event
String Kalender::getTimeNaechstes() {
    return Time[getIDNaechstes()];
}

// Function for getting the next event
String Kalender::getEventNaechstes() {
    if (Event[getIDNaechstes()] == "")
        return "Keine Termine";
    return Event[getIDNaechstes()];
}

// Function for getting the day of the next event
int Kalender::getDayNaechstes() {
    return Day[getIDNaechstes()];
}

// Function for getting the weekday of the next event
String Kalender::getWeekDayNaechstes() {
    return Wochentag(WeekDay[getIDNaechstes()]);
}

// Function for getting the month of the next event
String Kalender::getMonthNaechstes() {
    return Monat(Month[getIDNaechstes()]);
}

// Function for getting the current time
String Kalender::getTime() {
    return Time[0];
}

// Function for getting the current day
int Kalender::getDay() {
    return Day[0];
}

// Function for getting the current year
int Kalender::getYear() {
    return Year[0];
}

// Function for getting the current weekday
String Kalender::getWeekDay() {
    return Wochentag(WeekDay[0]);
}

// Function for getting the current month
String Kalender::getMonth() {
    return Monat(Month[0]);
}
