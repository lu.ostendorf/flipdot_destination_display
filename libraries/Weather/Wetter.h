#ifndef Wetter_h
#define Wetter_h

#include "Arduino.h"
#include <ESP8266WiFi.h>


class Wetter
{
  public:
    Wetter(WiFiClient client_s);
    void getWetter();
    int getTempJetzt();
    int getHighHeute();
    int getLowHeute();
    String getCondHeute();
    int getHighMorgen();
    int getLowMorgen();
    String getCondMorgen();
  private:
    String translate(String cond);
    const char hostname[20]={'q','u','e','r','y','.','y','a','h','o','o','a','p','i','s','.','c','o','m','\0'};
    const String url="Your Yahoo Wather API Link";
    const int port=80;
    unsigned long timeout;
    int temp;
    int high;
    int low;
    String condition;
    int highMorgen;
    int lowMorgen;
    String conditionMorgen;
    WiFiClient client;
};

#endif
