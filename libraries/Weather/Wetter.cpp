#include "Arduino.h"
#include "Wetter.h"
#include <ESP8266WiFi.h>

Wetter::Wetter(WiFiClient client_s) {
    // Create Weather Client
    client = client_s;
    hostname[] = "query.yahooapis.com";
    url = "Your Yahoo Wather API Link";
    port = 80;
    timeout = 10000;  // ms
}


// Function to retrieve weather information from yahoo weather (https://developer.yahoo.com/weather/)
void Wetter::getWetter() {
    unsigned long timestamp;

    // Establish TCP connection
    if (!client.connect(hostname, port)) {
        Serial.println("Connection failed");
    }

    // Send GET request
    String req = "GET " + url + " HTTP/1.1\r\n" +
                 "Host: " + hostname + "\r\n" +
                 "Connection: close\r\n" +
                 "\r\n";
    client.print(req);

    // Wait for response from server
    delay(500);
    timestamp = millis();
    while (!client.available() && (millis() < timestamp + timeout)) {
        delay(1);
    }

    // Parse temperature
    if (client.find("temp\":")) {
        temp = client.parseInt();
    }

    if (client.find("text\":\"")) {
        condition = client.readStringUntil('\"');
    }

    if (client.find("high\":")) {
        high = client.parseInt();
    }

    if (client.find("low\":")) {
        low = client.parseInt();
    }

    if (client.find("high\":")) {
        highMorgen = client.parseInt();
    }

    if (client.find("low\":")) {
        lowMorgen = client.parseInt();
    }

    if (client.find("text\":\"")) {
        conditionMorgen = client.readStringUntil('\"');
    }

    // Flush receive buffer
    while (client.available()) {
        client.readStringUntil('\r');
    }

    // Close TCP connection
    client.stop();
}

int Wetter::getTempJetzt() {
    return temp;
}

int Wetter::getHighHeute() {
    return high;
}

int Wetter::getLowHeute() {
    return low;
}

int Wetter::getHighMorgen() {
    return highMorgen;
}

int Wetter::getLowMorgen() {
    return lowMorgen;
}

String Wetter::getCondHeute() {
    return translate(condition);
}

String Wetter::getCondMorgen() {
    return translate(conditionMorgen);
}

// Translate to german
String Wetter::translate(String cond) {
    cond.replace("Showers", "Schauer");
    cond.replace("Rain", "Regen");
    cond.replace("Mostly Sunny", "meist Sonnig");
    cond.replace("Sunny", "Sonnenschein");
    cond.replace("Snow", "Schnee");
    cond.replace("Windy", "Windig");
    cond.replace("Cold", "Kalt");
    cond.replace("Hot", "Heiss");
    cond.replace("Scattered Thunderstorms", "Leichte Gewitter");
    cond.replace("Partly Cloudy", "Leicht Bew|lkt");
    cond.replace("Mostly Cloudy", "Stark Bew|lkt");
    cond.replace("Cloudy", "Bew|lkt");
    cond.replace("Scattered Showers", "Schauer");
    return cond;
}
