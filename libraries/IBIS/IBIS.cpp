/*
  IBIS.cpp - Library for IBIS FlipDot Signs.
  Created by Lukas Ostendorf, April 19, 2018.
*/

#include "Arduino.h"
#include <CustomSoftwareSerial.h>
#include "IBIS.h"

CustomSoftwareSerial *customSerial;

IBIS::IBIS(int rxpin, int txpin) {
    customSerial = new CustomSoftwareSerial(rxpin, txpin, true);    //rx, tx, invert
    customSerial->begin(1200, CSERIAL_7E2);                         //baud rate, parity
}

// Function to send Dataframes of type FMA-S1
void IBIS::sendDatasetFMA(String linie, String sonderzeichen, String Zeile1, String Groesse1, String Zeile2,
                          String Groesse2, String Liniengroesse) {
    // Join information to form a string. Format reverse engineered from IBISUtility
    // Message Start
    String telegram = "aA15";
    telegram.concat(LF);

    // Start of text
    telegram.concat(".W");
    telegram.concat(Zeile1);    // First line of text
    telegram.concat(LF);
    telegram.concat(Zeile2);    // Second line of text
    telegram.concat(LF);
    telegram.concat(" ");       // Not used
    telegram.concat(LF);
    telegram.concat(" ");       // Not used
    telegram.concat(LF);
    telegram.concat(" ");       // Not used
    telegram.concat(LF);
    telegram.concat(LF);

    // Start of Symbols
    telegram.concat(".Y");
    telegram.concat(linie);             // Line number
    telegram.concat(sonderzeichen);     // Special character (Football, Brandenburger Tor, usw.)
    telegram.concat("              ");  // Fill with empty lines
    telegram.concat(LF);
    telegram.concat(LF);

    // Start of formatting - this seems to differ between displays, so these values will only work for my display
    telegram.concat(".C");
    if (Zeile2 == " ")                              // Decision - Single line or Multi line
        telegram.concat("A");
    else
        telegram.concat("H");
    telegram.concat(Groesse1 + Groesse2);           // Page Size 1 (9: 16-24 Characters) (6: like 9 but up) (<: like 9 but bigger) (=: fine) (> normal) (? bold)
    telegram.concat("000000");                      // Page Size 2-4 (not used)
    telegram.concat("1");                           // Formatting of line number 1000,1100,1110,1111....
    telegram.concat("1");                           // Cycle time if cycling between pages is enabled 1000,1010....
    telegram.concat("1");                           // Pause representation 0001,0010,0100,1000
    telegram.concat("M");                           // Text alignment M - Middle, L - Left, R - Right
    telegram.concat("M");                           // Text alignment M - Middle, O - Upper, U - Lower
    telegram.concat("0");                           // Inverting 1000,1100.... (Depending on which page is inverted)
    telegram.concat("7");                           // Display Type (remains constant)
    telegram.concat(Liniengroesse);                 // Line Size Page 1  0: Auto Size
    telegram.concat("111");                         // Line Size Page 2-4

    // Send Message (and print on the serial monitor for debugging)
    sendMessage(telegram);
    Serial.println(telegram);
}

// Function to send Dataframes of type DS
void IBIS::sendDatasetDS(int linie = 0, char sonder = ' ', String text1raw = "", String text2raw = "") {

    // Set variables for protocol DS
    String ausgabelinie;
    String ausgabesonderzeichen;
    String ausgabetext;
    String ausgabetext2;
    String ausgabetext3;
    String text1 = getUmlaute(text1raw);
    String text2 = getUmlaute(text2raw);
    char textChar[text1.length()];
    char text2Char[text2.length()];

    // Get special characters
    int sonderzeichen = getSonderzeichen(sonder);

    // Formatting train number
    if (linie <= 9)
        ausgabelinie = "l00" + (String) linie;
    else if (linie <= 99)
        ausgabelinie = "l0" + (String) linie;
    else
        ausgabelinie = "l" + (String) linie;

    // Formatting special characters
    if (sonderzeichen <= 9)
        ausgabesonderzeichen = "lE0" + (String) sonderzeichen;
    else if (sonderzeichen <= 99)
        ausgabesonderzeichen = "lE" + (String) sonderzeichen;

    // Write text into string (differentiate between one or two lines)
    // Case 1: Only one Line
    if (text2 == "") {
        text1.getBytes(textChar, 32 + 1);
        ausgabetext2 = (String) textChar;
        int laenge = ausgabetext2.length();
        for (int i = 1; i <= (32 - laenge); i++)
            ausgabetext2.concat(" ");
    }
    // Case 2: Two Lines
    else {
        // Line 1
        text1.getBytes(textChar, 16 + 1);
        ausgabetext2 = (String) textChar;
        int laenge1 = ausgabetext2.length();
        for (int i = 1; i <= (16 - laenge1); i++)
            ausgabetext2.concat(" ");
        // Line 2
        text2.getBytes(text2Char, 16 + 1);
        ausgabetext3 = (String) text2Char;
        int laenge2 = ausgabetext3.length();
        for (int i = 1; i <= (16 - laenge2); i++)
            ausgabetext3.concat(" ");
        ausgabetext2 = ausgabetext2 + ausgabetext3;
    }

    // Concat and send message to the display
    ausgabetext = "zA2" + ausgabetext2;
    sendMessage(ausgabelinie);
    if (!ausgabelinie.equals("lE00"))
        sendMessage(ausgabesonderzeichen);
    sendMessage(ausgabetext);
    Serial.println(ausgabelinie);
    if (!ausgabelinie.equals("lE00"))
        Serial.println(ausgabesonderzeichen);
    Serial.println(ausgabetext);
}

// Function to send Dataframes over serial
void IBIS::sendMessage(String msg) {
    char msgChar[msg.length()];
    strcpy(msgChar, msg);
    for (byte i : msgChar)
        customSerial->write(i);                             // Message
    customSerial->write(Schlussbyte);                       // Closing byte
    customSerial->write(calculateParity(msg));              // Parity Byte (calculated)
}

// Function to calculate the parity of a message
byte IBIS::calculateParity(String msg) {
    char buf[(msg.length() + 1)];
    strcpy(buf, msg);
    buf[msg.length()] = Schlussbyte;
    char parity = 0x7F;
    for (int i = 0; i <= msg.length(); i++) {
        parity = parity ^ buf[i];
    }
    return parity;
}

// Function to replace "Umlaute" with the respective special character
String IBIS::getUmlaute(String text)
    text.replace("ä", "{");
    text.replace("ö", "|");
    text.replace("ü", "}");
    text.replace("Ä", "[");
    text.replace("Ö", "\\");
    text.replace("Ü", "]");
    text.replace("ß", "~");
    text.replace("UBAHN", "^");
    return text;
}

int IBIS::getSonderzeichen(char sonderzeichen) {
    int special_code;
    switch (sonderzeichen) {
        case 'E':
            special_code = 1;
            break;
        case '&':           //Kinder
            special_code = 13;
            break;
        case 'N':
            special_code = 14;
            break;
        case 'S':
            special_code = 5;
            break;
        case 'A':
            special_code = 6;
            break;
        case 'D':
            special_code = 11;
            break;
        case 'C':
            special_code = 12;
            break;
        case 'B':
            special_code = 13;
            break;
        case '/':
            special_code = 21;
            break;
        case 'U':
            special_code = 26;
            break;
        case 'M':
            special_code = 28;
            break;
        case '#':           //BVG
            special_code = 29;
            break;
        case '+':           //P+R
            special_code = 33;
            break;
        case 'X':
            special_code = 36;
            break;
        case '=':           //DB
            special_code = 37;
            break;
        case 'a':
            special_code = 41;
            break;
        case 'F':
            special_code = 53;
            break;
        case 'G':
            special_code = 54;
            break;
        case 'H':
            special_code = 55;
            break;
        case 'I':
            special_code = 56;
            break;
        case 'J':
            special_code = 57;
            break;
        case 'K':
            special_code = 58;
            break;
        case 'L':
            special_code = 59;
            break;
        case 'O':
            special_code = 62;
            break;
        case 'P':
            special_code = 63;
            break;
        case 'Q':
            special_code = 64;
            break;
        case 'R':
            special_code = 65;
            break;
        case 'T':
            special_code = 67;
            break;
        case 'V':
            special_code = 69;
            break;
        case 'W':
            special_code = 70;
            break;
        case 'Y':
            special_code = 72;
            break;
        case 'Z':
            special_code = 73;
            break;
        case '0':           //Fussball
            special_code = 78;
            break;
        case '^':           //Flugzeug
            special_code = 79;
            break;
        case 'm':           //Brandenburger Tor
            special_code = 80;
            break;
        case '*':           //SoftwareVersion
            special_code = 97;
            break;
        case 't':           //Test
            special_code = 99;
            break;
        default:
            special_code = 2;
            break;
    }
    return special_code;
}
