/*
  IBIS.h - Library for IBIS FlipDot Signs.
  Created by Lukas Ostendorf, April 19, 2018.
*/

#ifndef IBIS_h
#define IBIS_h

#include "Arduino.h"
#include <CustomSoftwareSerial.h>

class IBIS
{
  public:
    IBIS(int rxpin, int txpin);
    void sendDatasetFMA(String linie,String sonderzeichen,String Zeile1,String Groesse1,String Zeile2,String Groesse2,String Liniengroesse);
    void sendDatasetDS(int linie=0, char sonder=' ', String text1raw="", String text2raw="");
  private:
    void sendMessage(String msg);
    char calculateParity(String msg);
    String getUmlaute(String text);
    int getSonderzeichen(char sonderzeichen);

    // Constants
    byte Schlussbyte = 0x0D;

    // Dataset FMA Constants
    const char LF = 0x0A;      //LF Byte
    const char Liniennumerndarstellung = 0001;
    const char Taktzeit = 0001;
    const char Pause = 0001;
    const char Invertierung = 0000;
};

#endif
